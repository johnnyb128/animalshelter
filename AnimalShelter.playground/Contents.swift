import UIKit


enum Species:String,CustomStringConvertible{
    
    case Cat = "Cat"
    case Dog = "Dog"
    case Bird = "Bird"
    case Ferret = "Ferret"
    case Rabbit = "Rabbit"
    
    public var description:String{return "\(self.rawValue)"}
}

class Animal: CustomStringConvertible {
    
    var species:Species
    var name:String
    public var description:String{return "\(name):\(species)"}
    
    init(_ species:Species, name:String){
        self.species = species
        self.name = name
    }
    
    
}

class AnimalQueue: CustomStringConvertible {
    
    var queue:[Animal]
    public var description:String{return "\(queue.description)"}
    
    init(){
        queue = [Animal]()
    }
    
    
    func enqueue(animal:Animal){
        queue.append(animal)
    }
    
    func dequeue()->Animal{
        return queue.removeFirst()
    }
    
    func injectQueue(spot:Int, animal:Animal){
        queue.insert(animal, at: spot)
    }
    
    
    func removeAnimalAt(spot:Int)->Animal{
        return queue.remove(at: spot)
    }
    
    func animalAt(index:Int)->Animal?{
        if index >= 0 && index < queue.count{
            return queue[index]
        }
        
        return nil
    }
    
    
    func nextAdoption()->Animal?{
        return queue.removeFirst()
    }
    
    func size()->Int{
        return queue.count
    }
    
    
    func isEmpty()->Bool{
        return queue.isEmpty
    }
    
    
    subscript(range:Range<Int>)->ArraySlice<Animal>?{
        
        if range.lowerBound >= 0 && range.upperBound < self.queue.count{
            return queue[range.startIndex..<range.endIndex]
        }
        
        return nil
    }
    
}

infix operator ⇄

class AnimalShelter{
    
    var animals:[Species:Int]!
    var adoptionQueue:AnimalQueue!
    
    init(_ a:[Species:Int]){
        animals = a
        adoptionQueue = AnimalQueue()
    }
    
    init?(_ a:[Species:Int]?){
        if a == nil{
            return nil
        }
        
        animals = a
        adoptionQueue = AnimalQueue()
    }
    
    init(){
        animals = [Species:Int]()
        adoptionQueue = AnimalQueue()
    }
    
    subscript(index: Int) -> Animal? {
        get {
            return adoptionQueue.animalAt(index: index)
        }
        set(animal) {
            // Remove the old animal from adoption queue and animals count
            let removedAnimal = adoptionQueue.removeAnimalAt(spot: index)
            modifyAnimals(species: removedAnimal.species, val: -1)
            
            // Add the new animal to animals count and adoption queue
            modifyAnimals(species: animal!.species, val: 1)
            adoptionQueue.injectQueue(spot: index, animal: animal!)
        }
    }
    
    subscript(range:Range<Int>)->ArraySlice<Animal>?{
        
        return adoptionQueue![range.lowerBound..<range.upperBound]
    }
    
    
    func intakeNewAnimal(species:Species, name:String){
        
        let animal:Animal = Animal.init(species, name: name)
        adoptionQueue.enqueue(animal: animal)
        modifyAnimals(species: species, val: 1)
        
    }
    
    
    func adoptNextAnimal()->Animal{
        
        let nextAnimal = adoptionQueue.dequeue()
        modifyAnimals(species: nextAnimal.species, val: -1)
        
        return nextAnimal
    }
    
    private func modifyAnimals(species:Species, val:Int){
        
        if let _ = animals[species]{
            animals[species]! += val
        }else{
            animals[species] = 1
        }
        
    }
    
    
    static func +(leftShelter: AnimalShelter, rightShelter: AnimalShelter) -> AnimalShelter {
        
        var combinedShelters = [Species:Int]()
        
        // If both shelters have animials that have been initialized
        if let leftAnimals = leftShelter.animals, let rightAnimals = rightShelter.animals{
            
            combinedShelters = [Species:Int]()
            
            //Get the keys for both shelters and combine them
            let keysForBothShelters:Set<Species> = Set<Species>(Array(leftAnimals.keys)).union(Set<Species>(Array(rightAnimals.keys)))
            
            //Add the animal counts
            keysForBothShelters.map({key in
                // If both sides have values for the key
                if let leftAnimalCnt = leftAnimals[key], let rightAnimalCnt = rightAnimals[key]{
                    combinedShelters[key] = leftAnimalCnt + rightAnimalCnt
                }else{ //add the value from the side that has the key
                    combinedShelters[key] =  leftAnimals[key] == nil ? rightAnimals[key] : leftAnimals[key]
                }
            })
        }
        
        return AnimalShelter.init(combinedShelters)
    }
    
    
    static func +=(leftShelter: inout AnimalShelter, rightShelter: AnimalShelter){
        
        leftShelter = leftShelter + rightShelter
        
    }
    
    static func ⇄(leftShelter: AnimalShelter, rightShelter: AnimalShelter) {
        
        let t_animals = leftShelter.animals
        leftShelter.animals = rightShelter.animals
        rightShelter.animals = t_animals
        
    }
    
    
    
    
}


var as1 = AnimalShelter.init([Species.Cat:5, Species.Dog:10, Species.Bird:2, Species.Ferret:3])
var as2 = AnimalShelter.init([Species.Cat:10, Species.Dog:25, Species.Bird:5])
var as3 = AnimalShelter.init([Species.Cat:19, Species.Dog:33, Species.Bird:22, Species.Rabbit:16])
var combinedShelter = as1 + as2

print(combinedShelter.animals!)


combinedShelter += as3
print(combinedShelter.animals!)

print("----")
print(as1.animals!)
print(as2.animals!)
as1⇄as2

print(as1.animals!)
print(as2.animals!)
print("----")
var as4 = AnimalShelter.init()
as4.intakeNewAnimal(species: .Dog, name: "Fido")
as4.intakeNewAnimal(species: .Dog, name: "Spike")
as4.intakeNewAnimal(species: .Cat, name: "Fluffy")
as4.intakeNewAnimal(species: .Dog, name: "Rex")
as4.intakeNewAnimal(species: .Cat, name: "Buttons")

print(as4.animals)
print("\(as4[0]!.name):\(as4[0]!.species)")
print("\(as4[1]!.name):\(as4[1]!.species)")
print("\(as4[2]!.name):\(as4[2]!.species)")

print(as4.adoptionQueue!)

as4[1] = Animal.init(.Ferret, name: "Althea")
print(as4.animals)
print(as4.adoptionQueue!)
print(as4[0..<3])
as4[0..<33]
